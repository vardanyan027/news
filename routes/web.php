<?php

use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::post('/news/{item}/like', [NewsController::class, 'like'])->name('news.like');
Route::post('/news/{item}/dislike', [NewsController::class, 'dislike'])->name('news.dislike');
Route::get('/news', [NewsController::class, 'index'])->name('news.index');
