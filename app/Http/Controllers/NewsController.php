<?php

namespace App\Http\Controllers;

use App\Models\Dislike;
use App\Models\Like;
use App\Models\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $sortBy = $request->input('sort_by', 'rating'); // Default sorting by rating
        $news = News::orderBy($sortBy, 'desc')->paginate(10);
        foreach ($news as $item) {
            $item->tagName = join(", ", $item->tags->pluck('name')->toArray());
        }
        return view('news.index', compact('news'));
    }

    public function like(News $item)
    {
        $like = new Like();
        $like->news_id = $item->id;
        $like->save();

        $item->refreshRating();

        return redirect()->back();
    }

    public function dislike(News $item)
    {
        $dislike = new Dislike();
        $dislike->news_id = $item->id;
        $dislike->save();

        $item->refreshRating();

        return redirect()->back();
    }
}
