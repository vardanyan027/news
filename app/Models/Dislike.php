<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dislike extends Model
{
    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
