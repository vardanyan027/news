<?php

namespace App\Console\Commands;

use App\Models\News;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $xml = file_get_contents('https://lenta.ru/rss/news');
        $data = simplexml_load_string($xml);

        foreach ($data->channel->item as $item) {
            $news = News::where('guid', $item->guid)->first();
            if (!isset($news)) {
                $news = new News();
                $news->guid = $item->guid;
                $news->author = $item->author;
                $news->title = $item->title;
                $news->link = $item->link;
                $news->description = (string) $item->description;
                $news->pubDate = Carbon::parse($item->pubDate);
                $image = (string)$item->enclosure->attributes()['url'];
//                $news->image = file_get_contents($image);
                $news->image = 'test';
                $news->save();

                $category = $item->category;
                $tag = Tag::where('name', $category)->first();
                if (!isset($tag)) {
                    $tag = new Tag();
                    $tag->name = $category;
                    $tag->save();
                }

                $news->tags()->attach($tag->id);
            }
        }
    }
}
