<form action="{{ route('news.index') }}" method="GET">
    <label for="sort_by">Sort By:</label>
    <select name="sort_by" id="sort_by">
        <option value="rating" {{ Request::input('sort_by') == 'rating' ? 'selected' : '' }}>Rating</option>
        <option value="created_at" {{ Request::input('sort_by') == 'created_at' ? 'selected' : '' }}>Date Added</option>
    </select>
    <button type="submit">Sort</button>
</form>

@foreach ($news as $item)
    <div>
        <h3>{{ $item->title }}</h3>
        <img src="data:image/png;base64,{{base64_encode($item->image)}}" alt="Item Image" width="200">
        <p>{{ $item->description }}</p>
        <p>Rating: {{ $item->rating }}</p>
        <p>Tags: {{$item->tagName}}</p>

        <form action="{{ route('news.like', ['item' => $item->id]) }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-primary">Like</button>
        </form>

        <form action="{{ route('news.dislike', ['item' => $item->id]) }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-danger">Dislike</button>
        </form>
    </div>
@endforeach
